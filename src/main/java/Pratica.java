import java.lang.Runtime;
import java.lang.System;

public class Pratica {    
    public static void main(String args[]) {
            System.out.println("Sistema Operacional: " + System.getProperty("os.name"));
            System.out.println("Número de processadores: " + Runtime.getRuntime().availableProcessors());
            System.out.println("Memória total: " + (((Runtime.getRuntime().totalMemory())/1024.0)/1024.0) + " megabytes");
            System.out.println("Memória livre: " + (((Runtime.getRuntime().freeMemory())/1024.0)/1024.0) + " megabytes");
            System.out.println("Memória usada : " + (((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory())/1024.0)/1024.0) + " megabytes");   
            System.out.println("Memória máxima: " + (((Runtime.getRuntime().maxMemory())/1024.0)/1024.0) + " megabytes");
        }
}